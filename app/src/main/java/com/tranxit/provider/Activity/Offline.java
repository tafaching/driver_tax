package com.tranxit.provider.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.tranxit.provider.Helper.ConnectionHelper;
import com.tranxit.provider.Helper.CustomDialog;
import com.tranxit.provider.Helper.SharedHelper;
import com.tranxit.provider.Helper.URLHelper;
import com.tranxit.provider.Helper.XuberApplication;
import com.tranxit.provider.R;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

//public class Offline extends AppCompatActivity {
//
//    Activity activity = Offline.this;
//    Context context = Offline.this;
//    Button goOnlineBtn;
//    CustomDialog customDialog;
//    String token;
//
//    //menu icon
//    ImageView menuIcon;
//    int NAV_DRAWER = 0;
//    DrawerLayout drawer;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.activity_offline);
//        token = SharedHelper.getKey(activity, "access_token");
//        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        goOnlineBtn = (Button) findViewById(R.id.goOnlineBtn);
//        goOnlineBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                goOnline();
//            }
//        });
//    }
//
//    public void goOnline(){
//        customDialog = new CustomDialog(activity);
//        customDialog.setCancelable(false);
//        customDialog.show();
//        JSONObject param = new JSONObject();
//        try {
//            param.put("service_status", "active");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//       JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.UPDATE_AVAILABILITY_API ,param, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                customDialog.dismiss();
//                if (response != null) {
//                    if(response.optJSONObject("service").optString("status").equalsIgnoreCase("active")){
//                        Intent intent = new Intent(activity, MainActivity.class);
//                        startActivity(intent);
//                        finish();
//                    }else {
//                        displayMessage(getString(R.string.something_went_wrong));
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                customDialog.dismiss();
//                Log.v("Error", error.toString());
//                String json = null;
//                String Message;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null){
//
//                    try {
//                        JSONObject errorObj = new JSONObject(new String(response.data));
//
//                        if(response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500){
//                            try{
//                                displayMessage(errorObj.optString("message"));
//                            }catch (Exception e){
//                                displayMessage(getString(R.string.something_went_wrong));
//                            }
//                        }else if(response.statusCode == 401){
//                            SharedHelper.putKey(context,"loggedIn",getString(R.string.False));
//                            GoToBeginActivity();
//                        }else if(response.statusCode == 422){
//                            json = XuberApplication.trimMessage(new String(response.data));
//                            if(json !="" && json != null) {
//                                displayMessage(json);
//                            }else{
//                                displayMessage(getString(R.string.please_try_again));
//                            }
//
//                        }else if(response.statusCode == 503){
//                            displayMessage(getString(R.string.server_down));
//                        }else{
//                            displayMessage(getString(R.string.please_try_again));
//                        }
//
//                    }catch (Exception e){
//                        displayMessage(getString(R.string.something_went_wrong));
//                    }
//
//                }else{
//                    displayMessage(getString(R.string.please_try_again));
//                }
//            }
//        }){
//            @Override
//            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("X-Requested-With", "XMLHttpRequest");
//                headers.put("Authorization","Bearer "+token);
//                return headers;
//            }
//        };
//        XuberApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
//
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//
//    }
//
//    @Override
//    public void onBackPressed() {
//
//    }
//
//    public void displayMessage(String toastString){
//        Log.e("displayMessage",""+toastString);
//        Snackbar.make(getCurrentFocus(),toastString, Snackbar.LENGTH_SHORT)
//                .setAction("Action", null).show();
//    }
//
//    public void GoToBeginActivity(){
//        Intent mainIntent = new Intent(activity, BeginScreen.class);
//        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(mainIntent);
//        activity.finish();
//    }
//}


public class Offline extends Fragment {

    Activity activity;
    Context context;
    ConnectionHelper helper;
    Boolean isInternet;
    View rootView;
    CustomDialog customDialog;
    String token;
    Button goOnlineBtn;
    //menu icon
    ImageView menuIcon;
    int NAV_DRAWER = 0;
    DrawerLayout drawer;



    public Offline() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.activity_offline, container, false);
        findViewByIdAndInitialize();
        goOnlineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goOnline();
            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(NAV_DRAWER == 0) {
                    drawer.openDrawer(Gravity.LEFT);
                }else {
                    NAV_DRAWER = 0;
                    drawer.closeDrawers();
                }
            }
        });


        return  rootView;
    }

    public void findViewByIdAndInitialize(){
        helper = new ConnectionHelper(activity);
        isInternet = helper.isConnectingToInternet();
        token = SharedHelper.getKey(activity, "access_token");
        goOnlineBtn = (Button) rootView.findViewById(R.id.goOnlineBtn);
        menuIcon = (ImageView) rootView.findViewById(R.id.menuIcon);
        drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
    }


    public void goOnline(){
        customDialog = new CustomDialog(activity);
        customDialog.setCancelable(false);
        customDialog.show();
        JSONObject param = new JSONObject();
        try {
            param.put("service_status", "active");
        } catch (JSONException e) {
            e.printStackTrace();
        }
       JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.UPDATE_AVAILABILITY_API ,param, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                customDialog.dismiss();
                if (response != null) {
                    if(response.optJSONObject("service").optString("status").equalsIgnoreCase("active")){
                        Intent intent = new Intent(activity, MainActivity.class);
                        startActivity(intent);
                        activity.finish();
                    }else {
                        displayMessage(getString(R.string.something_went_wrong));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Log.v("Error", error.toString());
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){

                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if(response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500){
                            try{
                                displayMessage(errorObj.optString("message"));
                            }catch (Exception e){
                                displayMessage(getString(R.string.something_went_wrong));
                            }
                        }else if(response.statusCode == 401){
                            SharedHelper.putKey(context,"loggedIn",getString(R.string.False));
                            GoToBeginActivity();
                        }else if(response.statusCode == 422){
                            json = XuberApplication.trimMessage(new String(response.data));
                            if(json !="" && json != null) {
                                displayMessage(json);
                            }else{
                                displayMessage(getString(R.string.please_try_again));
                            }

                        }else if(response.statusCode == 503){
                            displayMessage(getString(R.string.server_down));
                        }else{
                            displayMessage(getString(R.string.please_try_again));
                        }

                    }catch (Exception e){
                        displayMessage(getString(R.string.something_went_wrong));
                    }

                }else{
                    displayMessage(getString(R.string.please_try_again));
                }
            }
        }){
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization","Bearer "+token);
                return headers;
            }
        };
        XuberApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void displayMessage(String toastString){
        Log.e("displayMessage",""+toastString);
        Snackbar.make(getView(),toastString, Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    public void GoToBeginActivity(){
        SharedHelper.putKey(activity,"loggedIn",getString(R.string.False));
        Intent mainIntent = new Intent(activity, BeginScreen.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        activity.finish();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
