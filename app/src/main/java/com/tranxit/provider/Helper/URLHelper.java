package com.tranxit.provider.Helper;

/**
 * Created by Taf on 26/12/16.
 */

public class URLHelper {
    public static String base = "http://10.0.0.160:8081/taxi/public/";
    public static String login = base+"api/provider/oauth/token";
    public static String register = base+"api/provider/register";
    public static String USER_PROFILE_API = base+"api/provider/profile";
    public static String UPDATE_AVAILABILITY_API = base+"api/provider/profile/available";
    public static String GET_HISTORY_API = base+"api/provider/requests/history";
    public static String GET_HISTORY_DETAILS_API = base+"api/provider/requests/history/details";
    public static String CHANGE_PASSWORD_API = base+"api/provider/profile/password";
}
